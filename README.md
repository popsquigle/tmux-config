# Welcome!
This is my tmux config! Follow the instructions below and you will be off to the races!

## Instructions
1. Clone repo
2. Profit?

# Bonus info!
This section is for me (popsquigle)
to use the yank tool:
1. enter copy mode ( prefix + [ )
2. use vim keys to go to the line you want
3. press 'v' to open line select mode
4. press 'y' to yank the selected lines to the clipboard
